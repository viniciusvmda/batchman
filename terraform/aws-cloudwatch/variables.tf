variable "service_prefix" { default = "service" }
variable "target_arn" {}
variable "schedule_expression" {}
variable "invoke_role_arn" {}
variable "job_definition_arn" {}