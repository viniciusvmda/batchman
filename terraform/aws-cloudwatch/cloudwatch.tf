resource "aws_cloudwatch_event_rule" "cloudwatch_event_rule" {
  name                = "${var.service_prefix}-cloudwatch_event_rule"
  schedule_expression = "${var.schedule_expression}"
}

resource "aws_cloudwatch_event_target" "cloudwatch_event_target" {
  rule  = "${aws_cloudwatch_event_rule.cloudwatch_event_rule.name}"
  arn   = "${var.target_arn}"
  role_arn = "${var.invoke_role_arn}"
  batch_target {
    job_definition                  = "${var.job_definition_arn}"
    job_name                        = "${var.service_prefix}-cloudwatch-target"
  }
}