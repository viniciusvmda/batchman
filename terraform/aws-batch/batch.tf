resource "random_string" "random" {
  length = 6
  special = false
  upper = false
}

resource "aws_batch_compute_environment" "batch_compute_environment" {
  compute_environment_name = "${var.service_prefix}-batch-compute-environment-${random_string.random.result}"
  compute_resources {
    instance_role = "${aws_iam_instance_profile.batch_instance_profile.arn}"
    instance_type = ["optimal"]

    min_vcpus = 0
    max_vcpus = 2
    desired_vcpus = 0

    security_group_ids = ["${aws_security_group.security_group.id}"]
    subnets = ["${aws_default_subnet.subnet_a.id}","${aws_default_subnet.subnet_b.id}", "${aws_default_subnet.subnet_c.id}"]

    type = "EC2"
  }
  
  lifecycle {
    create_before_destroy = true
  }

  service_role = "${aws_iam_role.batch_service_role.arn}"
  type         = "MANAGED"
  depends_on   = ["aws_iam_role_policy_attachment.batch_service_attachment"]
}

resource "null_resource" "job_definition_depends_on" {
  triggers = {
    deps = "${jsonencode(var.container_depends_on)}"
  }
}

resource "aws_batch_job_definition" "batch_job_definition" {
  name = "${var.service_prefix}-batch-job-definition"
  depends_on   = ["null_resource.job_definition_depends_on"]
  type = "container"
  container_properties = "${var.container_definition}"
}

resource "aws_batch_job_queue" "batch_job_queue" {
  name                 = "${var.service_prefix}-batch-job-queue"
  state                = "ENABLED"
  priority             = 10
  compute_environments = ["${aws_batch_compute_environment.batch_compute_environment.arn}"]
}

output "batch_job_queue_arn" { value = "${aws_batch_job_queue.batch_job_queue.arn}"}
output "batch_job_definition_arn" { value = "${aws_batch_job_definition.batch_job_definition.arn}"}