# resource "aws_security_group" "security_group" {
#   name = "${var.service_prefix}-security-group"
#   vpc_id     = aws_vpc.default_vpc.id
#   ingress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }

# resource "aws_vpc" "default_vpc" {
#   cidr_block = "10.1.0.0/16"
# }

# resource "aws_subnet" "subnet" {
#   vpc_id     = aws_vpc.default_vpc.id
#   cidr_block = "10.1.1.0/24"
#   map_public_ip_on_launch = true
# }

resource "aws_default_subnet" "subnet_a" {
  availability_zone = "us-east-1a"
}

resource "aws_default_subnet" "subnet_b" {
  availability_zone = "us-east-1b"
}

resource "aws_default_subnet" "subnet_c" {
  availability_zone = "us-east-1c"
}

resource "aws_security_group" "security_group" {
  name = "${var.service_prefix}-security-group"
  vpc_id     = aws_default_vpc.default_vpc.id
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_default_vpc" "default_vpc" {
}