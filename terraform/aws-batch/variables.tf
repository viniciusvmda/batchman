variable "service_prefix" { default = "service" }
variable "container_definition" { default = "" }
variable "container_depends_on" { 
    default = []
    type = list(string)
}