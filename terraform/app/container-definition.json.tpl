{
    "mountPoints": [], 
    "image": "${app_image}", 
    "environment": [
        {
            "name": "ENV_VAR_EXAMPLE", 
            "value": "${ENV_VAR_EXAMPLE}"
        }
    ], 
    "vcpus": ${app_vcpus},
    "jobRoleArn": "${job_role_arn}", 
    "volumes": [], 
    "memory": ${app_memory}, 
    "resourceRequirements": [], 
    "command": [
        "java", 
        "-jar", 
        "/app/app.jar"
    ],
    "ulimits": []
}