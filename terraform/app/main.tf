terraform {
  backend "s3" {
    region = "us-east-1"
  }
}

provider "aws" {
  region = "us-east-1"
}

data "template_file" "container_definition" {
  template = "${file("${path.module}/container-definition.json.tpl")}"
  vars = {
    ENV_VAR_EXAMPLE             = "variavel de ambiente"
    app_vcpus                   = "1"
    app_memory                  = "1024"
    app_image                   = "${var.REPOSITORY_URL}:${var.IMAGE_TAG}"
    job_role_arn                = "${module.batch.batch_task_execution_role_name}"
  }
}

module "batch" {
  source                            = "../aws-batch"
  service_prefix                    = "${var.service_prefix}"
  container_definition              = "${data.template_file.container_definition.rendered}"
  container_depends_on              = ["data.template_file.container_definition"]
}

module "cloudwatch" {
  source                            = "../aws-cloudwatch"
  service_prefix                    = "${var.service_prefix}"
  target_arn                        = "${module.batch.batch_job_queue_arn}"
  schedule_expression               = "${var.SCHEDULE_EXPRESSION}"
  invoke_role_arn                   = "${module.batch.batch_invoke_role_arn}"
  job_definition_arn                = "${module.batch.batch_job_definition_arn}"
}