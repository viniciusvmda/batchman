variable "STATE_BUCKET_NAME" { default = "" }
variable "STATE_BUCKET_KEY" { default = "" }
variable "AWS_DEFAULT_REGION" { default = "" }
variable "AWS_ACCESS_KEY_ID" { default = "" }
variable "AWS_SECRET_ACCESS_KEY" { default = "" }
variable "REPOSITORY_URL" { default = "" }
variable "IMAGE_TAG" { default = "latest" }
variable "service_prefix" { default = "batchman" }

variable "SCHEDULE_EXPRESSION" { default = "cron(16 47 ? * * *)" }